# Nu-du

## Docker
Linux:

`docker run -v /home/mariusz/photo_app:/data -p 8080:8080 -t pl.mariuszgezicki/photo-app --spring.profiles.active=docker`

Windows:

`docker run -v c:\photo_app\:/data -p 8080:8080 -t pl.mariuszgezicki/photo-app --spring.profiles.active=docker`

## Endpoints

### `PUT` `/register` returns user information
```
{
  email: string,
  password: string
}
```

### `POST` `/auth` returns JWT string
```
{
  email: string,
  password: string
}
```

## Secured endpoints
Secured endpoints require JWT to be present in the header as follows:
`Authentication: Bearer <JWT>`

### `PUT` `/gallery` returns photo information
```
{
    file: File
}
```

### `GET` `/gallery?page=0&direction=DESC&sort=uploaded&search=` returns list of user photos, 
- `page` parameter is optional
- `direction` parameter is optional, allowed `ASC` and `DESC`
- `sort` parameter is optional, allowed `uploaded` and `name`
- `search` parameter is optional, file name to search

### `GET` `/gallery/{fileName}` returns image of a given name

### `DELETE` `/gallery/{fileName}` deletes image

### `PATCH` `/user/email` - changes user email
- put new email as plain text request body
