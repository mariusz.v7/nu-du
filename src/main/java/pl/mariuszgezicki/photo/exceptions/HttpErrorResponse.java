package pl.mariuszgezicki.photo.exceptions;

import org.springframework.http.HttpStatus;

public class HttpErrorResponse {
    private final HttpStatus status;
    private final String error;
    private final String message;

    public HttpErrorResponse(HttpStatus status, String message) {
        this.status = status;
        this.error = status.getReasonPhrase();
        this.message = message;
    }

    public int getStatus() {
        return status.value();
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
