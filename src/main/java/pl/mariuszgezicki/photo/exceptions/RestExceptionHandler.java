package pl.mariuszgezicki.photo.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.mariuszgezicki.photo.gallery.FileStoreException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Optional;


@ControllerAdvice
public class RestExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public HttpErrorResponse validationException(MethodArgumentNotValidException e) {
        logger.warn("", e);

        String errMsg = e.getBindingResult().getFieldError().getDefaultMessage();
        Object rejectedValue = e.getBindingResult().getFieldError().getRejectedValue();
        if (rejectedValue != null) {
            errMsg = errMsg.replace("{value}", rejectedValue.toString());
        }

        return new HttpErrorResponse(HttpStatus.BAD_REQUEST, errMsg);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public HttpErrorResponse constraintViolationException(ConstraintViolationException e) {
        logger.warn("", e);

        Optional<ConstraintViolation<?>> constraintViolation = e.getConstraintViolations().stream().findAny();
        if (!constraintViolation.isPresent()) {
            return new HttpErrorResponse(HttpStatus.BAD_REQUEST, "");
        }

        String errMsg = constraintViolation.get().getMessage();
        if (constraintViolation.get().getInvalidValue() != null) {
            errMsg = errMsg.replace("{value}", constraintViolation.get().getInvalidValue().toString());
        }


        return new HttpErrorResponse(HttpStatus.BAD_REQUEST, errMsg);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public HttpErrorResponse illegalArgumentException(IllegalArgumentException e) {
        logger.warn("", e);

        return new HttpErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public HttpErrorResponse authenticationCredentialsNotFoundException(AuthenticationCredentialsNotFoundException e) {
        logger.warn("", e);

        return new HttpErrorResponse(HttpStatus.UNAUTHORIZED, e.getMessage());
    }

    @ExceptionHandler(FileStoreException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public HttpErrorResponse fileStoreException(FileStoreException e) {
        logger.warn("", e);

        return new HttpErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}
