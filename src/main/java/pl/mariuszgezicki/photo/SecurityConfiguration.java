package pl.mariuszgezicki.photo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.mariuszgezicki.photo.user.UserService;

import java.security.Key;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserService userService;
    private final Key jwtKey;
    private final PasswordEncoder passwordEncoder;
    private final ObjectMapper objectMapper;

    SecurityConfiguration(UserService userService, PasswordEncoder passwordEncoder, Key jwtKey, ObjectMapper objectMapper) {
        this.userService = userService;
        this.jwtKey = jwtKey;
        this.passwordEncoder = passwordEncoder;
        this.objectMapper = objectMapper;
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .addFilterAfter(new JwtAuthenticationFilter(jwtKey, objectMapper), UsernamePasswordAuthenticationFilter.class)
                .csrf().disable()
                .sessionManagement().disable()
                .authorizeRequests()
                .antMatchers("/auth", "/register").permitAll()
                .anyRequest().authenticated();
    }

}
