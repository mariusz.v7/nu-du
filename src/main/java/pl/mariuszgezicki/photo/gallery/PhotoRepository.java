package pl.mariuszgezicki.photo.gallery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

interface PhotoRepository extends PagingAndSortingRepository<Photo, Long> {
    Optional<Photo> findByOwnerAndName(long owner, String name);

    Page<Photo> findByOwner(long owner, Pageable pageable);

    Page<Photo> findByOwnerAndNameContaining(long owner, String name, Pageable pageable);
}
