package pl.mariuszgezicki.photo.gallery;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.stream.Stream;

@Service
class FileStorageService {
    private final String storageDirectory;
    private final String fileFormat = "jpeg";

    FileStorageService(@Value("${photo.storage_dir}") String storageDirectory) {
        this.storageDirectory = storageDirectory;
    }

    byte[] getImage(long ownerId, String fileName) throws IOException {
        Path userDir = getUserDir(ownerId);
        Path imagePath = Paths.get(userDir.toString(), fileName);

        try (FileInputStream inputStream = new FileInputStream(imagePath.toFile())) {
            int size = inputStream.available();
            byte[] bytes = new byte[size];
            inputStream.read(bytes);
            return bytes;
        }
    }

    String getThumbnailAsBase64(long ownerId, String fileName) throws IOException {
        Path fileLocation = Paths.get(getUserThumbs(ownerId).toString(), fileName);

        File file = fileLocation.toFile();

        try (FileInputStream inputStream = new FileInputStream(file)) {
            int size = inputStream.available();
            byte[] bytes = new byte[size];

            inputStream.read(bytes);

            return Base64.getEncoder().encodeToString(bytes);
        }
    }

    String saveImage(long ownerId, String fileName, InputStream inputStream) throws IOException {
        BufferedImage image = ImageIO.read(inputStream);
        if (image == null) {
            throw new IllegalArgumentException("Uploaded file is not an image!");
        }

        Path userDir = getUserDir(ownerId);
        Path thumbnailDir = getUserThumbs(ownerId);
        Path imagePath = Paths.get(userDir.toString(), fileName);
        Path thumbnailPath = Paths.get(thumbnailDir.toString(), fileName);

        createDirsIfNotExist(userDir, thumbnailDir);

        try {
            saveImage(image, imagePath);
            saveThumbnail(image, thumbnailPath);
        } catch (RuntimeException e) {
            imagePath.toFile().delete();
            thumbnailPath.toFile().delete();
            throw e;
        }

        return fileName;
    }

    void deleteImage(long ownerId, String fileName) {
        Path userDir = getUserDir(ownerId);
        Path thumbnailDir = getUserThumbs(ownerId);
        Path imagePath = Paths.get(userDir.toString(), fileName);
        Path thumbnailPath = Paths.get(thumbnailDir.toString(), fileName);

        imagePath.toFile().delete();
        thumbnailPath.toFile().delete();
    }

    private void saveImage(BufferedImage bufferedImage, Path path) throws IOException {
        ImageIO.write(bufferedImage, fileFormat, path.toFile());
    }

    private void saveThumbnail(BufferedImage bufferedImage, Path path) throws IOException {
        int scale = 50;
        int scaleW, scaleH;

        if (bufferedImage.getWidth() == bufferedImage.getHeight()) {
            scaleW = scale;
            scaleH = scale;
        } else if (bufferedImage.getWidth() > bufferedImage.getHeight()) {
            int ratio = bufferedImage.getWidth() / scale;
            scaleW = scale;
            scaleH = bufferedImage.getHeight() / ratio;
        } else {
            int ratio = bufferedImage.getHeight() / scale;
            scaleW = bufferedImage.getWidth() / ratio;
            scaleH = scale;
        }

        BufferedImage scaled = new BufferedImage(scaleW, scaleH, bufferedImage.getType());
        Graphics2D g = scaled.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.drawImage(bufferedImage, 0, 0, scaleW, scaleH, null);
        g.dispose();

        ImageIO.write(scaled, fileFormat, path.toFile());
    }

    private void createDirsIfNotExist(Path... paths) {
        Stream.of(paths)
                .forEach(path -> path.toFile().mkdirs());
    }

    private Path getUserDir(long ownerId) {
        return Paths.get(storageDirectory, String.valueOf(ownerId));
    }

    private Path getUserThumbs(long ownerId) {
        return Paths.get(storageDirectory, String.valueOf(ownerId), "thumbs");
    }
}
