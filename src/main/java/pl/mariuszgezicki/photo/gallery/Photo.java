package pl.mariuszgezicki.photo.gallery;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Entity
@Table(indexes = @Index(name = "owner_idx", columnList = "owner, name", unique = true))
public class Photo {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;

    private String name;

    private long owner;

    private LocalDateTime uploaded;

    public Photo(String name, long owner) {
        this.name = name;
        this.owner = owner;
        this.uploaded = LocalDateTime.now(ZoneId.of("UTC"));
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getOwner() {
        return owner;
    }

    public LocalDateTime getUploaded() {
        return uploaded;
    }

    private Photo() {
    }
}
