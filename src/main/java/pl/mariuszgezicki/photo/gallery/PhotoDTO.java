package pl.mariuszgezicki.photo.gallery;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class PhotoDTO {
    private final long id;
    private final String name;
    private final ZonedDateTime uploaded;
    private final String thumbnail;

    public PhotoDTO(long id, String name, LocalDateTime uploaded, String thumbnail) {
        this.id = id;
        this.name = name;
        this.uploaded = uploaded.atZone(ZoneId.of("UTC"));
        this.thumbnail = thumbnail;
    }

    public long getId() {
        return id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getName() {
        return name;
    }

    public ZonedDateTime getUploaded() {
        return uploaded;
    }
}
