package pl.mariuszgezicki.photo.gallery;

public class FileStoreException extends RuntimeException {
    public FileStoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
