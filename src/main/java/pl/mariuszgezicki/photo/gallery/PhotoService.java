package pl.mariuszgezicki.photo.gallery;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
public class PhotoService {
    private final PhotoRepository photoRepository;
    private final FileStorageService fileStorageService;
    private final int pageSize;

    PhotoService(PhotoRepository photoRepository, FileStorageService fileStorageService, @Value("${photo.page_size}") int pageSize) {
        this.photoRepository = photoRepository;
        this.fileStorageService = fileStorageService;
        this.pageSize = pageSize;
    }

    public Page<Photo> getUserImages(long userId, int page, Sort.Direction direction, String sort, String search) {
        if (!search.isEmpty()) {
            return photoRepository.findByOwnerAndNameContaining(userId, search, PageRequest.of(page, pageSize, direction, sort));
        } else {
            return photoRepository.findByOwner(userId, PageRequest.of(page, pageSize, direction, sort));
        }
    }

    @Transactional
    public String saveImage(long ownerId, MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (photoRepository.findByOwnerAndName(ownerId, fileName).isPresent()) {
            throw new IllegalArgumentException("File " + fileName + " already exists");
        }

        photoRepository.save(new Photo(fileName, ownerId));

        try {
            return fileStorageService.saveImage(ownerId, fileName, file.getInputStream());
        } catch (IOException e) {
            throw new FileStoreException("Failed to save file", e);
        }
    }

    public byte[] getImage(long ownerId, String fileName) {
        if (!photoRepository.findByOwnerAndName(ownerId, fileName).isPresent()) {
            throw new IllegalArgumentException("File " + fileName + " does not exist");
        }

        try {
            return fileStorageService.getImage(ownerId, fileName);
        } catch (IOException e) {
            throw new FileStoreException("Failed to read file", e);
        }
    }

    @Transactional
    public void deleteImage(long ownerId, String fileName) {
        Optional<Photo> file = photoRepository.findByOwnerAndName(ownerId, fileName);
        if (!file.isPresent()) {
            throw new IllegalArgumentException("File " + fileName + " does not exist");
        }

        photoRepository.delete(file.get());
        fileStorageService.deleteImage(ownerId, fileName);
    }

    public PhotoDTO toPhotoDTO(Photo photo) {
        try {
            return new PhotoDTO(photo.getId(), photo.getName(), photo.getUploaded(), fileStorageService.getThumbnailAsBase64(photo.getOwner(), photo.getName()));
        } catch (IOException e) {
            throw new FileStoreException("Failed to read file", e);
        }
    }
}
