package pl.mariuszgezicki.photo.gallery;

import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/gallery")
@Validated
public class GalleryController {
    private final PhotoService photoService;

    GalleryController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping
    public List<PhotoDTO> myImages(
            @RequestParam(value = "page", required = false, defaultValue = "0")
                    int page,
            @Pattern(regexp = "^(DESC|ASC)$", message = "Allowed values for direction are: DESC, ASC")
            @RequestParam(value = "direction", required = false, defaultValue = "DESC")
                    String direction,
            @Pattern(regexp = "^(name|uploaded)$", message = "Allowed values for sort are: uploaded, name")
            @RequestParam(value = "sort", required = false, defaultValue = "uploaded")
                    String sort,
            @RequestParam(value = "search", required = false, defaultValue = "")
                    String search
    ) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        long user = (long) authentication.getPrincipal();

        return photoService.getUserImages(user, page, Sort.Direction.valueOf(direction), sort, search).stream()
                .map(photoService::toPhotoDTO)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{name}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImage(@PathVariable("name") String fileName) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        long user = (long) authentication.getPrincipal();

        return photoService.getImage(user, fileName);
    }

    @PutMapping
    public String uploadImage(@RequestParam("file") MultipartFile file) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        long user = (long) authentication.getPrincipal();

        return photoService.saveImage(user, file);
    }

    @DeleteMapping("{name}")
    public void deleteImage(@PathVariable("name") String fileName) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        long user = (long) authentication.getPrincipal();

        photoService.deleteImage(user, fileName);
    }

}
