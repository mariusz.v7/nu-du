package pl.mariuszgezicki.photo;

public class AppConstants {
    public static final String JWT_HEADER_NAME = "Authentication";
    public static final String JWT_HEADER_PREFIX = "Bearer";
    public static final String JWT_USER_CLAIM_KEY = "user";
}
