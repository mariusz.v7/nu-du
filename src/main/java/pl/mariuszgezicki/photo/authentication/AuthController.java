package pl.mariuszgezicki.photo.authentication;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mariuszgezicki.photo.AppConstants;
import pl.mariuszgezicki.photo.user.UserDetails;

import java.security.Key;
import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final Key jwtKey;
    private final Clock clock;

    AuthController(AuthenticationManager authenticationManager, Key jwtKey, Clock clock) {
        this.authenticationManager = authenticationManager;
        this.jwtKey = jwtKey;
        this.clock = clock;
    }

    @PostMapping
    public String authentcate(@RequestBody LoginForm loginForm) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginForm.getEmail(), loginForm.getPass()));

        UserDetails details = (UserDetails) authentication.getPrincipal();

        return Jwts.builder()
                .signWith(jwtKey)
                .claim(AppConstants.JWT_USER_CLAIM_KEY, details.getUser().getId())
                .setExpiration(Date.from(Instant.now(clock).plus(4, ChronoUnit.HOURS)))
                .compact();
    }
}
