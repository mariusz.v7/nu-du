package pl.mariuszgezicki.photo.authentication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginForm {
    private final String email;
    private final String pass;

    @JsonCreator
    public LoginForm(@JsonProperty("email") String email, @JsonProperty("password") String pass) {
        this.email = email;
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public String getPass() {
        return pass;
    }
}
