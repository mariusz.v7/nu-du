package pl.mariuszgezicki.photo.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO {
    private final long id;
    private final String email;

    @JsonCreator
    public UserDTO(@JsonProperty("id") long id, @JsonProperty("email") String email) {
        this.id = id;
        this.email = email;
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
