package pl.mariuszgezicki.photo.user;

import java.util.Collections;

public class UserDetails extends org.springframework.security.core.userdetails.User {
    private final User user;

    public UserDetails(User user) {
        super(user.getEmail(), user.getPassword(), Collections.emptyList());
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
