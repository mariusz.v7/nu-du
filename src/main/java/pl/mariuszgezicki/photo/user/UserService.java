package pl.mariuszgezicki.photo.user;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public User createUser(String email, String password) {
        Optional<User> user = userRepository.findByEmail(email);
        if (user.isPresent()) {
            throw new IllegalArgumentException("Email " + email + " already exists");
        }

        String encoded = passwordEncoder.encode(password);

        return userRepository.save(new User(email, encoded));
    }

    @Transactional
    public User changeUserEmail(long userId, String newEmail) {
        Optional<User> user = userRepository.findByEmail(newEmail);
        if (user.isPresent()) {
            throw new IllegalArgumentException("Email " + newEmail + " already exists");
        }

        Optional<User> currentUser = userRepository.findById(userId);
        if (!currentUser.isPresent()) {
            throw new IllegalArgumentException("Invalid user id");
        }

        currentUser.get().setEmail(newEmail);

        return userRepository.save(currentUser.get());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByEmail(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException("User not found");
        }

        return new UserDetails(user.get());
    }
}
