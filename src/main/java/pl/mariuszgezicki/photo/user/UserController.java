package pl.mariuszgezicki.photo.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/user")
@Validated
public class UserController {
    private final UserService userService;

    UserController(UserService userService) {
        this.userService = userService;
    }

    @PatchMapping("/email")
    public void changeEmail(@RequestBody(required = false) // just to handle it by "@NotEmpty"
                            @Email(message = "Invalid email: {value}")
                            @NotEmpty(message = "Email cannot be empty")
                                    String email) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        long user = (long) authentication.getPrincipal();

        userService.changeUserEmail(user, email);
    }
}
