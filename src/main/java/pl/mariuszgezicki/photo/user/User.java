package pl.mariuszgezicki.photo.user;

import javax.persistence.*;

@Entity
@Table(name = "users") // "user" is reserved keyword in postgres
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String email;

    private String password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDTO toDTO() {
        return new UserDTO(this);
    }

    private User() {
    }
}
