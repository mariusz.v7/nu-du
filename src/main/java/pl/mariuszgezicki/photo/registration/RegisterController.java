package pl.mariuszgezicki.photo.registration;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mariuszgezicki.photo.user.UserDTO;
import pl.mariuszgezicki.photo.user.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/register")
public class RegisterController {
    private final UserService userService;

    RegisterController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping
    public UserDTO createNewUser(@RequestBody @Valid RegisterForm registerForm) {
        return userService.createUser(registerForm.getEmail(), registerForm.getPassword()).toDTO();
    }
}
