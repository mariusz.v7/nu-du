package pl.mariuszgezicki.photo.registration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class RegisterForm {
    @Email(message = "Invalid email: {value}")
    @NotEmpty(message = "Email cannot be empty")
    private final String email;

    @NotNull(message = "Password cannot be empty")
    @Length(min = 8, max = 20, message = "Password must contain at least 8 characters and at most 20 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*]).*$", message = "Password must contain at least one upper case letter, one lower case letter, one number and one special character")
    private final String password;

    @JsonCreator
    public RegisterForm(@JsonProperty("email") String email, @JsonProperty("password") String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "RegisterForm{" +
                "email='" + email + '\'' +
                ", password='*******'" +
                '}';
    }
}
