package pl.mariuszgezicki.photo;

import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.Clock;
import java.util.Base64;

@SpringBootApplication
public class PhotoApplication {
    private static final Logger logger = LoggerFactory.getLogger(PhotoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(PhotoApplication.class, args);
    }

    @Configuration
    static class Cfg {

        @Bean
        PasswordEncoder passwordEncoder() {
            return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        }

        @Bean
        Key jwtKey(@Value("${photo.authentication_key}") String encodedKey) {
            byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
            return new SecretKeySpec(decodedKey, 0, decodedKey.length, SignatureAlgorithm.HS256.getJcaName());
        }

        @Bean
        Clock clock() {
            return Clock.systemUTC();
        }

        @Bean
        MethodValidationPostProcessor methodValidationPostProcessor() {
            return new MethodValidationPostProcessor();
        }
    }

    @Profile("docker")
    @Configuration
    @PropertySource("file:/data/config.properties") // docker
    static class DockerCfg {
        DockerCfg() {
            logger.info("DOCKER CONFIGURATION");
        }
    }

    @Profile("dev")
    @Configuration
    @PropertySource("file:config/config.properties") // docker
    static class DevCfg {
        DevCfg() {
            logger.info("DEVELOPMENT CONFIGURATION");
        }
    }

}

