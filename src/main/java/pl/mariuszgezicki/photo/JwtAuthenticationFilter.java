package pl.mariuszgezicki.photo;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Key;
import java.util.Collections;

public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Key key;
    private final ObjectMapper objectMapper;

    public JwtAuthenticationFilter(Key key, ObjectMapper objectMapper) {
        this.key = key;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String header = request.getHeader(AppConstants.JWT_HEADER_NAME);
            if (header == null || !header.startsWith(AppConstants.JWT_HEADER_PREFIX)) {
                SecurityContextHolder.clearContext();
                return;
            }

            String token = header.replace(AppConstants.JWT_HEADER_PREFIX, "").trim();
            Claims claims = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token)
                    .getBody();

            long userId = claims.get(AppConstants.JWT_USER_CLAIM_KEY, Long.class);

            SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userId, null, Collections.emptyList()));
        } catch (RuntimeException e) {
            SecurityContextHolder.clearContext();
            logger.debug("Failed to authenticate", e);
        } finally {
            filterChain.doFilter(request, response);
        }
    }
}
