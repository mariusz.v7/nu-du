CREATE TABLE users
(
  id bigserial NOT NULL,
  email character varying(255),
  password character varying(255),
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT email_unique UNIQUE (email)
);

CREATE TABLE photo
(
  id bigserial NOT NULL,
  name character varying(255),
  owner bigint NOT NULL REFERENCES users(id),
  uploaded timestamp without time zone,
  CONSTRAINT photo_pkey PRIMARY KEY (id),
  CONSTRAINT owner_idx UNIQUE (owner, name)
);
