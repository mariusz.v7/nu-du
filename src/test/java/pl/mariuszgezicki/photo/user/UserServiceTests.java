package pl.mariuszgezicki.photo.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTests {
    private final long user1Id = 2;
    private final String user1Email = "user1@example.com";
    private final String rawPassword = "raw password";
    private final String encodedPassword = "encoded password";

    private final String nonExistingEmal = "no@email.com";

    private final User user1 = new User(user1Email, encodedPassword);

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @BeforeEach
    void before() {
        userRepository = mock(UserRepository.class);
        passwordEncoder = mock(PasswordEncoder.class);

        when(userRepository.findByEmail(user1Email)).thenReturn(Optional.of(user1));
        when(userRepository.findByEmail(nonExistingEmal)).thenReturn(Optional.empty());

        when(userRepository.findById(user1Id)).thenReturn(Optional.of(user1));

        when(passwordEncoder.encode(rawPassword)).thenReturn(encodedPassword);
        when(passwordEncoder.matches(rawPassword, encodedPassword)).thenReturn(true);

        userService = new UserService(userRepository, passwordEncoder);
    }

    @Test
    void givenEmailExists_whenCreateUser_thenException() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> userService.createUser(user1Email, ""));
        assertThat(e.getMessage()).isEqualTo("Email " + user1Email + " already exists");
    }

    @Test
    void givenEmailOk_whenCreateNewUser_thenSaveWithEncodedPassword() {
        String newEmail = "new-email@example.com";
        userService.createUser(newEmail, "raw password");

        ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);

        verify(userRepository).save(captor.capture());

        User captured = captor.getValue();
        assertThat(captured.getEmail()).isEqualTo(newEmail);
        assertThat(captured.getPassword()).isEqualTo(encodedPassword);
    }

    @Test
    void givenRepositoryReturnUserOnSave_whenCreateNewUser_thenReturnThatValue() {
        User user = mock(User.class);
        when(userRepository.save(any(User.class))).thenReturn(user);

        User result = userService.createUser("any@example.com", "raw password");

        assertThat(result).isSameAs(user);
    }

    @Test
    void givenUserDoesNotExist_whenLoadByUserName_thenException() {
        UsernameNotFoundException e = assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername(nonExistingEmal));
        assertThat(e.getMessage()).isEqualTo("User not found");
    }

    @Test
    void givenUserIsPresent_whenLoadByUserName_thenReturnUserDetails() {
        UserDetails userDetails = userService.loadUserByUsername(user1Email);
        assertThat(userDetails.getUser()).isSameAs(user1);
    }

    @Test
    void givenEmailExists_whenChangeEmail_thenException() {
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> userService.changeUserEmail(1, user1Email));
        assertThat(e.getMessage()).isEqualTo("Email " + user1Email + " already exists");
    }

    @Test
    void givenEmailDoesNotExist_whenChangeEmail_thenSave() {
        String newEmail = "new@email.com";
        userService.changeUserEmail(user1Id, newEmail);
        verify(userRepository).save(user1);
        assertThat(user1.getEmail()).isEqualTo(newEmail);
    }

}
