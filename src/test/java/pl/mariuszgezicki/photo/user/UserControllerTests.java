package pl.mariuszgezicki.photo.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.mariuszgezicki.photo.AppConstants;
import pl.mariuszgezicki.photo.exceptions.HttpErrorResponse;

import java.security.Key;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@TestPropertySource("file:config/config.properties")
class UserControllerTests {
    private long userId = 12;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private Key jwtKey;

    @MockBean
    private UserService userService;

    private String jwt;

    @BeforeEach
    void before() {
        jwt = Jwts.builder()
                .signWith(jwtKey)
                .claim(AppConstants.JWT_USER_CLAIM_KEY, userId)
                .compact();
    }

    @Test
    void givenInvalidEmail_whenUpdate_thenError() throws Exception {
        assertErrorResponse("Invalid", "Invalid email: Invalid");
    }

    @Test
    void givenEmptyEmail_whenUpdate_thenError() throws Exception {
        assertErrorResponse("", "Email cannot be empty");
    }

    @Test
    void givenEmailOk_whenUpdate_thenDelegate_toUserService() throws Exception {
        String newEmail = "newmail@example.com";

        mvc.perform(patch("/user/email")
                .contentType(MediaType.TEXT_PLAIN)
                .content(newEmail)
                .header("Authentication", "Bearer " + jwt))
                .andExpect(status().isOk());

        verify(userService).changeUserEmail(userId, newEmail);
    }

    private void assertErrorResponse(String email, String message) throws Exception {
        HttpErrorResponse expected = new HttpErrorResponse(HttpStatus.BAD_REQUEST, message);

        mvc.perform(patch("/user/email")
                .contentType(MediaType.TEXT_PLAIN)
                .content(email)
                .header("Authentication", "Bearer " + jwt))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapper.writeValueAsString(expected)));
    }
}
