package pl.mariuszgezicki.photo.registration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.mariuszgezicki.photo.exceptions.HttpErrorResponse;
import pl.mariuszgezicki.photo.user.User;
import pl.mariuszgezicki.photo.user.UserDTO;
import pl.mariuszgezicki.photo.user.UserService;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(RegisterController.class)
@TestPropertySource("file:config/config.properties")
class RegisterControllerTests {
    private final String validPassword = "ValidPassword!1";
    private final String validEmail = "valid@email.com";

    private final User user = new User(validEmail, validPassword);

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper mapper;

    @BeforeEach
    void before() {
        when(userService.createUser(validEmail, validPassword)).thenReturn(user);
    }

    @Test
    void givenEmailEmpty_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm("", validPassword);

        assertErrorResponse(form, "Email cannot be empty");
    }

    @Test
    void givenEmailNull_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(null, validPassword);

        assertErrorResponse(form, "Email cannot be empty");
    }

    @Test
    void givenInvalidEmail1_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm("invalid", validPassword);

        assertErrorResponse(form, "Invalid email: invalid");
    }

    @Test
    void givenInvalidEmail2_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm("@", validPassword);

        assertErrorResponse(form, "Invalid email: @");
    }

    @Test
    void givenPasswordIsNull_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, null);

        assertErrorResponse(form, "Password cannot be empty");
    }

    @Test
    void givenPasswordLessThan8Chars_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, "!2Eapc7");

        assertErrorResponse(form, "Password must contain at least 8 characters and at most 20 characters");
    }

    @Test
    void givenPasswordMoreThan20Chars_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, "!2Eapc789012345678901");

        assertErrorResponse(form, "Password must contain at least 8 characters and at most 20 characters");
    }

    @Test
    void givenPasswordWithoutUpperLetter_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, "12345678a!");

        assertErrorResponse(form, "Password must contain at least one upper case letter, one lower case letter, one number and one special character");
    }

    @Test
    void givenPasswordWithoutLowerLetter_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, "12345678A!");

        assertErrorResponse(form, "Password must contain at least one upper case letter, one lower case letter, one number and one special character");
    }

    @Test
    void givenPasswordWithoutSpecialChar_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, "12345678Aa");

        assertErrorResponse(form, "Password must contain at least one upper case letter, one lower case letter, one number and one special character");
    }

    @Test
    void givenPasswordWithoutNumber_whenCreateNewUser_thenError() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, "aBcDeFgh!");

        assertErrorResponse(form, "Password must contain at least one upper case letter, one lower case letter, one number and one special character");
    }

    @Test
    void givenAllOk_whenCreateNewUser_thenOk() throws Exception {
        RegisterForm form = new RegisterForm(validEmail, validPassword);

        mvc.perform(put("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(form)))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(new UserDTO(0, validEmail))));
    }

    private void assertErrorResponse(RegisterForm form, String message) throws Exception {
        HttpErrorResponse expected = new HttpErrorResponse(HttpStatus.BAD_REQUEST, message);

        mvc.perform(put("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(form)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapper.writeValueAsString(expected)));
    }
}
