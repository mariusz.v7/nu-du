package pl.mariuszgezicki.photo.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.mariuszgezicki.photo.AppConstants;
import pl.mariuszgezicki.photo.exceptions.HttpErrorResponse;
import pl.mariuszgezicki.photo.user.User;
import pl.mariuszgezicki.photo.user.UserDetails;
import pl.mariuszgezicki.photo.user.UserService;

import java.security.Key;
import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AuthController.class)
@TestPropertySource("file:config/config.properties")
class AuthControllerTests {
    private final String invalidEmail = "invalid@example.com";
    private final String invalidPass = "invalid pass";
    private final String validEmail = "valid@example.com";
    private final String validPass = "valid pass";
    private final User user = new User(validEmail, validPass);

    private final UserDetails userDetails = new UserDetails(user);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private Key jwtKey;

    @MockBean
    private UserService userService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Clock clock;

    @BeforeEach
    void before() {
        when(userService.loadUserByUsername(invalidEmail)).thenThrow(new IllegalArgumentException("User not found"));
        when(userService.loadUserByUsername(validEmail)).thenReturn(userDetails);

        when(passwordEncoder.encode(validPass)).thenReturn(validPass);
        when(passwordEncoder.matches(validPass, validPass)).thenReturn(true);
    }

    @Test
    void givenInvalidCredentials_whenAuthenticate_thenError() throws Exception {
        HttpErrorResponse expected = new HttpErrorResponse(HttpStatus.BAD_REQUEST, "User not found");

        LoginForm loginForm = new LoginForm(invalidEmail, invalidPass);

        mvc.perform(post("/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginForm)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapper.writeValueAsString(expected)));
    }

    @Test
    void givenValidCredentials_whenAuthenticate_thenReturnToken() throws Exception {
        LoginForm loginForm = new LoginForm(validEmail, validPass);

        String expected = Jwts.builder()
                .signWith(jwtKey)
                .claim(AppConstants.JWT_USER_CLAIM_KEY, user.getId())
                .setExpiration(Date.from(Instant.now(clock).plus(4, ChronoUnit.HOURS)))
                .compact();

        mvc.perform(post("/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginForm)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(content().string(expected));
    }

}
